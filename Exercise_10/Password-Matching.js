

function verify(){
  var empty = (document.passwords.Password.value && document.passwords.Confirm.value) ? false : true;
  var match = (document.passwords.Password.value == document.passwords.Confirm.value) ? true : false;

  if(!empty && match){
    document.getElementById('misNotif').setAttribute('hidden', true);
    document.getElementById('Go').disabled = false;
  } else {
    document.getElementById('misNotif').removeAttribute('hidden');
    document.getElementById('Go').disabled = true;
  }
}
