function getAge(DOB){
  var today = new Date();
  var birthDate = new Date(DOB);
  var age = today.getFullYear() - birthDate.getFullYear();
  var m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age = age - 1;
  }

  var cage = document.getElementById("age");
  cage.value = age;
}

function preview(file){
  preview.scr = file;
  console.log(file);
}
