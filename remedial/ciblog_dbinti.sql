CREATE DATABASE IF NOT EXISTS ciblog;

use ciblog;

CREATE TABLE posts (
  id            int(11)           AUTO_INCREMENT,
  title         varchar(255)      not null,
  slug          varchar(255)      not null,
  body          text              not null,
  created_at    timestamp         default CURRENT_TIMESTAMP,

  constraint primary key (id)
);

CREATE TABLE user_creds (
  username         varchar(100)      not null,
  password         varchar(255)      not null,

  constraint primary key (username)
);

INSERT INTO posts (title, slug, body) VALUES
('Lorem Ipsum', '5c98a44b67657', '
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ornare, mauris mattis facilisis auctor, enim est iaculis erat, sodales ornare erat est in tortor. Vestibulum venenatis ex augue, id ultrices lorem tincidunt sed. Donec vitae ex scelerisque, aliquet massa vitae, cursus magna. Duis vestibulum nulla posuere porta ullamcorper. Aenean id dui accumsan, dapibus arcu ac, facilisis elit. Phasellus congue efficitur ligula non tempor. Sed lobortis erat eget mi dignissim vestibulum. Quisque feugiat nibh nibh, non lobortis dui suscipit sed. Integer laoreet purus leo, et faucibus dolor luctus non. Sed feugiat vulputate tempor. Sed suscipit quis nisi ut tincidunt. Morbi eu ante vitae metus porttitor condimentum.

Donec sed erat molestie magna ullamcorper vestibulum. Vestibulum pharetra lorem sollicitudin quam lacinia, ac sagittis risus feugiat. Vivamus mollis mattis nibh, vel suscipit turpis ultricies sit amet. Cras dolor sem, bibendum et bibendum eget, viverra ut augue. Maecenas in tincidunt odio, ut pellentesque sem. Sed leo ante, auctor in congue eget, vehicula quis ipsum. Donec ornare augue pretium diam laoreet, sed efficitur lorem auctor. Donec in augue aliquet, vulputate tellus non, pharetra purus. Aliquam cursus risus felis, vitae rutrum mauris interdum ut. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in vestibulum metus. Aenean sollicitudin velit nibh, eget hendrerit risus dignissim ut. Integer ultrices molestie lectus at tempor. Sed quis dolor eget lorem interdum dignissim vitae efficitur lectus. Duis sodales vehicula imperdiet. Proin venenatis erat ante, et cursus urna convallis quis.

Nulla mattis tincidunt risus vel mollis. Suspendisse convallis nisi non lorem imperdiet ornare. Praesent nec vulputate lectus. Aenean rhoncus at velit vel pulvinar. Aliquam euismod, nisi eu lacinia accumsan, odio augue accumsan mi, eu lacinia nibh metus non mi. Suspendisse vel est lacus. Nulla velit tortor, interdum nec venenatis molestie, lobortis pretium augue. Morbi tempus dui a ante consectetur lobortis. Duis lobortis enim nec elit hendrerit, vitae faucibus ex ultrices. Proin eget dictum libero. Donec quis elit elementum, porta purus et, varius velit.

Nam vestibulum, mi vitae convallis mattis, lorem ante vestibulum est, sit amet fringilla erat ipsum ut ipsum. Suspendisse posuere diam sed dui mollis, in posuere ante dapibus. Nunc urna elit, commodo sed tincidunt at, ultricies id metus. Sed nec urna euismod, scelerisque orci eget, hendrerit tortor. Curabitur vel nisl massa. Curabitur sed lorem neque. Praesent in bibendum ante. Curabitur eu tristique velit. Maecenas consectetur ligula eu elit semper, eu faucibus ante dignissim. Nam aliquet commodo lorem nec cursus. Suspendisse a rhoncus nisi. Curabitur vehicula enim ut consectetur rhoncus.

Etiam lacinia pretium metus vitae volutpat. Curabitur eget pretium neque. Praesent commodo sapien sed justo euismod venenatis tristique at sem. Maecenas at tincidunt justo, et placerat neque. Aenean vehicula nunc id leo elementum porttitor. Sed iaculis vel nibh aliquet tempor. Vestibulum eu mattis eros.

Etiam vel dolor at nisl vestibulum euismod. Integer tincidunt a nulla ac commodo. Sed eleifend facilisis faucibus. Sed ultrices tortor non odio pretium tincidunt. Phasellus varius lacinia urna eu auctor. Donec iaculis, lorem molestie vehicula luctus, elit magna vulputate enim, sit amet finibus dolor nisi vel felis. Praesent tempor lectus purus, sed vulputate ipsum fermentum eu. Nullam nec fermentum magna. Nunc vehicula blandit ante, ut fermentum ligula.

Donec ornare arcu a mi semper porttitor. Maecenas ornare sed sem a iaculis. Integer quis maximus velit. Cras egestas velit sit amet auctor interdum. Quisque a nulla ipsum. Duis placerat mauris vel nulla tempor facilisis. Sed porttitor consectetur turpis eu condimentum. Suspendisse ac sodales odio, tempor consequat nisl. Integer quis volutpat lacus. Curabitur auctor quam id ligula lacinia, sit amet fermentum justo vulputate. Sed posuere arcu non pellentesque ullamcorper.

Sed vel erat lectus. Integer dignissim eros in iaculis gravida. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam luctus eros facilisis, porttitor neque id, placerat purus. Cras non mollis turpis. Fusce dictum est eget pulvinar facilisis. Mauris non purus eget lectus vestibulum ornare a ut metus. Nulla facilisi. Proin laoreet, ipsum sit amet semper laoreet, lectus dui ultricies sapien, id placerat lorem dui ut odio.

Cras luctus facilisis quam. Aliquam fermentum porttitor lectus mattis tempor. Sed ornare bibendum massa, non viverra nisi. Praesent hendrerit fringilla velit vel volutpat. Nulla eu elementum enim. Nullam urna ligula, eleifend et leo ut, mollis eleifend risus. Nullam malesuada nisi vitae eros consequat dignissim. Maecenas faucibus venenatis massa suscipit volutpat. Nam eget auctor ante. Nulla volutpat fringilla libero, non molestie mi efficitur nec. Maecenas sit amet augue id elit vestibulum maximus. Integer et augue mauris. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi ac urna facilisis, bibendum est ut, pharetra sem. Maecenas ornare sollicitudin leo sit amet scelerisque. Praesent eu tincidunt tortor.

Aliquam lacinia elit in lacus suscipit, eget fermentum elit dignissim. Proin vel justo eget neque tempus auctor consectetur rhoncus tellus. Ut semper efficitur ante, porta luctus nisi cursus at. Maecenas a neque quam. Suspendisse posuere lobortis dolor, eget finibus lectus vestibulum sit amet. Maecenas rhoncus lobortis justo vel sollicitudin. Maecenas condimentum risus non elit semper pulvinar. Vivamus mi felis, pretium non lobortis ut, egestas eget nisl. Cras gravida porta lorem, quis cursus urna dictum eu. Sed vitae lobortis lorem. Donec facilisis neque nec metus congue, ac tincidunt velit ultrices. Morbi augue nisi, posuere ac sagittis ac, imperdiet in arcu. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam vulputate eros egestas, iaculis dolor vitae, vulputate ipsum. Interdum et malesuada fames ac ante ipsum primis in faucibus. ');

INSERT INTO user_creds (username, password) VALUES
('test', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8');

COMMIT;
