var curr = window.location.href;
var exploded = curr.split("/");

if(exploded[exploded.length-1] == "?!"){
  $('#login-alert').removeClass('alert-success').addClass('alert-danger')
  .html("<strong>Your credentials doesn't ring a bell</strong>. Can you try that again.").removeAttr('hidden');
}
