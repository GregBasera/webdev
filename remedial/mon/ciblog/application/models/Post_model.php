<?php
  class Post_model extends CI_Model{
    public function __construct(){
      $this->load->database();
    }

    public function get_posts($slug = FALSE){
      if($slug === FALSE){
        $this->db->order_by("created_at", "desc");
        $query = $this->db->get('posts');
        return $query->result_array();
      }

      $query = $this->db->get_where('posts', array('slug' => $slug));
      return $query->row_array();
    }

    public function set_post($title, $body){
      $data = array(
        'title' => $title,
        'slug' => uniqid(),
        'body' => $body
      );

      return ($this->db->insert('posts', $data)) ? true : false;
    }
  }
