<?php
class Login_model extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function validate($user){
    $this->db->select('password');
    $this->db->from('user_creds');
    $this->db->where('username', $user);

    $query = $this->db->get();
    return $query->result_array();
  }
}
