<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logins extends CI_Controller {
  public function index(){
    $data['title'] = 'Login';

    $this->load->view('templates/login-header');
    $this->load->view('pages/login', $data);
    $this->load->view('templates/footer');
  }

  public function logout(){
    $_SESSION['user'] = "";
    $_SESSION['pass'] = "";
    redirect('/', 'refresh');
  }
}
