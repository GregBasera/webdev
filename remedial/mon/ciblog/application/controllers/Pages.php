<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {
	public function view($page = 'home'){
		$user = $this->input->post('user');
    $pass = $this->input->post('pass');
    $pass = sha1($pass);

    if(empty($user) || empty($pass)){
      $user = $_SESSION['user'];
      $pass = $_SESSION['pass'];
    }

		$_SESSION['user'] = $user;
		$_SESSION['pass'] = $pass;

    $validate = $this->login_model->validate($_SESSION['user']);
		if(empty($validate)) redirect('/logins/index/?!', 'refresh');
    if($pass != $validate[0]['password'])
			redirect('/logins/index/?!', 'refresh');

    $data['title'] = ucfirst($page);

    $this->load->view('templates/header');
    $this->load->view('pages/' . $page, $data);
    $this->load->view('templates/footer');
	}
}
