<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends CI_Controller {
	public function index(){
		$validate = $this->login_model->validate($_SESSION['user']);
		if(empty($validate)) redirect('/logins/index/?!', 'refresh');
    if($_SESSION['pass'] != $validate[0]['password'])
			redirect('/logins/index/?!', 'refresh');

    $data['title'] = "Latest Posts";
    $data['posts'] = $this->post_model->get_posts();

    $this->load->view('templates/header');
    $this->load->view('posts/index', $data);
    $this->load->view('templates/footer');
	}

  public function view($slug = NULL){
		$validate = $this->login_model->validate($_SESSION['user']);
		if(empty($validate)) redirect('/logins/index/?!', 'refresh');
    if($_SESSION['pass'] != $validate[0]['password'])
			redirect('/logins/index/?!', 'refresh');

		$data['post'] = $this->post_model->get_posts($slug);
		if(empty($data['post'])){
			show_404();
		}

		$data['title'] = $data['post']['title'];

    $this->load->view('templates/header');
    $this->load->view('posts/view', $data);
    $this->load->view('templates/footer');
  }

	public function upload(){
		$post_title = $this->input->post('title');
		$post_body = $this->input->post('body');

		if($this->post_model->set_post($post_title, $post_body)){
			redirect('/posts', 'refresh');
		} else {
			$this->load->view('errors/cli/error_db.php');
		}
  }
}
