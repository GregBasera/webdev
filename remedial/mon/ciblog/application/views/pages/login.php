<div class="my-2">
  <h2><?php echo $title ?></h2>
</div>
<form class="mt-5" action="<?php echo base_url(); ?>pages/view" method="post">
  <div class="row">
    <div class="col-5">
      <input class="form-control" type="text" name="user" value="" placeholder="Username">
    </div>
    <div class="col-5">
      <input class="form-control" type="password" name="pass" value="" placeholder="Password">
    </div>
    <div class="col-2">
      <input class="btn btn-success btn-block" type="submit" name="log" value="Log in">
    </div>
  </div>
</form>
<div class="cotainer-fluid mt-2">
  <div class="alert alert-dismissible fade show" role="alert" hidden id="login-alert"></div>
</div>
