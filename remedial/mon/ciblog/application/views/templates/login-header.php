<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>CODEIGNITER BLOG</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/darkly/bootstrap.min.css" integrity="sha384-w+8Gqjk9Cuo6XH9HKHG5t5I1VR4YBNdPt/29vwgfZR485eoEJZ8rJRbm3TR32P6k" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css"> -->
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-light bg-light">
      <div class="container">
        <div class="navbar-brand">
          <a href="<?php base_url(); ?>" class="navbar-brand">CIblog</a>
        </div>
      </div>
    </nav>

    <div class="container">
