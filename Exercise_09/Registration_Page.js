function extractAnswers(){
  var extracted = {
    fname:document.register.Firstname.value,
    lname:document.register.Lastname.value,
    email:document.register.Email.value,
    gender:document.register.Gender.value,
    birthdate:document.register.Birthdate.value,
    // profilePic:document.register.ProfilePic.value,
    username:document.register.Username.value,
    password:document.register.Password.value,
    rePassword:document.register.ConPassword.value
  };

  // console.log(extracted);

  document.getElementById("dfname").innerHTML = extracted.fname;
  document.getElementById("dlname").innerHTML = extracted.lname;
  document.getElementById("demail").innerHTML = extracted.email;
  document.getElementById("dgender").innerHTML = extracted.gender;
  document.getElementById("dbdate").innerHTML = extracted.birthdate;
  // document.getElementById("dppic").innerHTML = extracted.fname;
  document.getElementById("dusername").innerHTML = extracted.username;

  if(extracted.password == extracted.rePassword && extracted.password && extracted.rePassword){
    document.getElementById("dconfirm").innerHTML = "true";
  } else {
    document.getElementById("dconfirm").innerHTML = "false";
  }

  if(extracted.fname && extracted.lname && extracted.email && extracted.gender && extracted.birthdate && extracted.username && document.getElementById("dconfirm").innerHTML == "true"){
    document.getElementById("dregistered").innerHTML = "true";
    document.getElementById("dregistered").classList.remove("bg-danger");
    document.getElementById("dregistered").classList.add("bg-success");
  } else {
    document.getElementById("dregistered").innerHTML = "false";
    document.getElementById("dregistered").classList.remove("bg-success");
    document.getElementById("dregistered").classList.add("bg-danger");
  }
}
