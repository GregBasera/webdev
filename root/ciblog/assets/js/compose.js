function verify(){
  // this function is called on every 'keyup' from the user
  // it checks the value inside the input elements and determines if the submit
  // button should be disabled or not. If one of the input elements is empty, the buttom should be disabled
  // if the 'title' element exceeded 255 characters, the button should also be disabled
  var title = document.compose_form.title.value;
  var body = document.compose_form.body.value;

  document.getElementById('submit').disabled = (title == '' || body == '' || title.length > 255) ? true : false;
  document.getElementById('title_indi').hidden = (title.length > 255) ? false : true;
}

function confirmClicked(){
  document.getElementById('status').hidden = false;
}

console.log("compose.js loaded");
