<?php
  class Post_model extends CI_Model{
    public function __construct(){
      // loads the database
      $this->load->database();
    }

    public function get_posts($slug = FALSE){
      // this is the mechanism behind 'posts/index' and 'posts/view'
      // when $slug is false it calls a procedure inside the database that
      // returns all items form 'posts' table inside 'ciblog' database.
      // when $slug is NOT false it searches the database for an entry with a
      // matching $slug
      if($slug === FALSE){
        // $this->db->order_by("created_at", "desc");
        $query = $this->db->query('CALL getPosts();');
        return $query->result_array();
      }

      $query = $this->db->get_where('posts', array('slug' => $slug));
      return $query->row_array();
    }

    public function set_post($title, $body){
      // this is the mechanism behind 'posts/upload'
      // 'posts/upload' will pass the values here ($title, $body)
      // this function will then generate a unique id as slug and finally,
      // insert it in the database. if the insertion is successful this function
      // returns 'true' if not it returns 'false'
      $data = array(
        'title' => $title,
        'slug' => uniqid('', true),
        'body' => $body
      );

      return ($this->db->insert('posts', $data)) ? true : false;
    }
  }
