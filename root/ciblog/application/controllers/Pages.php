<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {
	public function view($page = 'home'){
		// this controller handles page requests for 'home', 'about' and other pages
		// it simply calls the views that correspond to the request made.
		// if the requested page exist in the file system it proceeds to dispay
		// if not it prompts 404
		if(!file_exists(APPPATH . 'views/pages/' . $page . '.php'))
      show_404();

    $data['title'] = ucfirst($page);

    $this->load->view('templates/header');
    $this->load->view('pages/' . $page, $data);
    $this->load->view('templates/footer');
	}
}
