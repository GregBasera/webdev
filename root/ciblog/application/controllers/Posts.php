<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends CI_Controller {
	public function index(){
		// this function gets all items inside table 'posts' in the database ciblog
		// and asks the view 'posts/index' to display them. It uses the function 'get_posts()'
		// from the post_model
    $data['title'] = "Latest Posts";
    $data['posts'] = $this->post_model->get_posts();

    $this->load->view('templates/header');
    $this->load->view('posts/index', $data);
    $this->load->view('templates/footer');
	}

  public function view($slug = NULL){
		// this function gets a specific item inside posts identified by 'slug' and
		// asks the view 'posts/view' to display it. It uses the function 'get_posts($slug)'
		// from the post_model
		$data['post'] = $this->post_model->get_posts($slug);
		if(empty($data['post'])){
			show_404();
		}

		$data['title'] = $data['post']['title'];

    $this->load->view('templates/header');
    $this->load->view('posts/view', $data);
    $this->load->view('templates/footer');
  }

	public function upload(){
		// when submit button is clicked, the form data is packaged as a POST request
		// this function then access the POST request by indexes
		// and then passes it to Post_model so it can be inserted to the database
		// if the model came back successful the user is redirected to '/posts'
		// if not a database error is prompted
		$post_title = $this->input->post('title');
		$post_body = $this->input->post('body');

		if($this->post_model->set_post($post_title, $post_body)){
			redirect('/posts', 'refresh');
		} else {
			$this->load->view('errors/cli/error_db.php');
		}
  }
}
