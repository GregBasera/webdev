<div class="jumbotron py-4 mt-4 text-justify">
  <h3><?php echo $post['title']; ?></h3>
  <!-- just display the first 10 characters of the TIME_STAMP -->
  <small class="text-muted">Posted on: <?php echo substr($post['created_at'], 0, 10); ?></small>
  <div class="container my-4">
    <?php echo $post['body']; ?>
  </div>
</div>
