<div class="my-2">
  <h2>Compose</h2>
</div>
<form class="my-4" target="" action="<?php base_url(); ?>posts/upload" method="post" name="compose_form">
  <div class="form-group row">
    <label for="title" class="col-sm-1 col-form-label">Title</label>
    <div class="col-sm-11">
      <input type="text" class="form-control" value="" name="title" onkeyup="verify()">
    </div>
  </div>
  <textarea class="form-control" rows="10" name="body" onkeyup="verify()" style="resize:none"></textarea>
  <div class="container-fluid d-flex justify-content-end my-3">
    <small class="text-danger my-auto mx-2" id="title_indi" hidden>Title is limited to 255 characters</small>
    <div class="spinner-border my-auto mx-2" id="status" role="status" onclick="confirmClicked()" hidden>
      <span class="sr-only">Loading...</span>
    </div>
    <input class="btn btn-lg btn-success" id="submit" type="submit" value="Submit" disabled>
  </div>
</form>
