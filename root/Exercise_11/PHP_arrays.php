<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>PHP_Arrays</title>

    <style media="screen">
      .light{
        background-color: #E8E8E8;
      }
      .dark{
        background-color: #D0D0D0;
      }
    </style>
  </head>
  <?php
    $countries = array( "Italy"=>"Rome", "China"=>"Beijing", "Belgium"=> "Brussels", "Denmark"=>"Copenhagen", "Finland"=>"Helsinki", "France"=>"Paris", "Slovakia"=>"Bratislava", "Slovenia"=>"Ljubljana", "Germany"=>"Berlin", "philippines"=>"manila",
    "Greece"=>"Athens", "Ireland"=>"Dublin", "Netherlands"=>"Amsterdam", "Portugal"=>"Lisbon", "Spain"=>"Madrid", "Sweden"=>"Stockholm", "United Kingdom"=>"London", "Cyprus"=>"Nicosia", "Colombia"=>"Bogota", "Czech Republic"=>"Prague", "Japan"=>"Tokyo",
    "Hungary"=>"Budapest", "Zimbabwe"=>"Harare", "Libya"=>"Tripoli", "austriA"=>"VieNna", "Poland"=>"Warsaw");
    $sorted = uksort($countries, 'strcasecmp');
  ?>
  <body>
    <?php
      $sentinel = 0;
      foreach ($countries as $key => $value) {
        if($sentinel % 2 == 0){
          echo "<div class=light>The capital of ".ucfirst(strtolower($key))." is ".ucfirst(strtolower($value))."</div>";
        } else {
          echo "<div class=dark>The capital of ".ucfirst(strtolower($key))." is ".ucfirst(strtolower($value))."</div>";
        }
        $sentinel++;
      }
    ?>
  </body>
</html>
